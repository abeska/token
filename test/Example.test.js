const Example = artifacts.require("Example");

contract('Example', (accounts) => {
  it('should put 10000 Example in the first account', async () => {
    const exampleInstance = await Example.deployed();
    const balance = await exampleInstance.getBalance.call(accounts[0]);

    assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
  });
  it('should send coin correctly', async () => {
    const exampleInstance = await Example.deployed();

    // Setup 2 accounts.
    const accountOne = accounts[0];
    const accountTwo = accounts[1];

    // Get initial balances of first and second account.
    const accountOneStartingBalance = (await exampleInstance.getBalance.call(accountOne)).toNumber();
    const accountTwoStartingBalance = (await exampleInstance.getBalance.call(accountTwo)).toNumber();

    // Make transaction from first account to second.
    const amount = 10;
    await exampleInstance.sendCoin(accountTwo, amount, { from: accountOne });

    // Get balances of first and second account after the transactions.
    const accountOneEndingBalance = (await exampleInstance.getBalance.call(accountOne)).toNumber();
    const accountTwoEndingBalance = (await exampleInstance.getBalance.call(accountTwo)).toNumber();


    assert.equal(accountOneEndingBalance, accountOneStartingBalance - amount, "Amount wasn't correctly taken from the sender");
    assert.equal(accountTwoEndingBalance, accountTwoStartingBalance + amount, "Amount wasn't correctly sent to the receiver");
  });
});
