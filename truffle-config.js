const HDWalletProvider = require('@truffle/hdwallet-provider');                                                                                                                                                                                         
const privateKeys = [
	"a7d690453b9495ed3e27063a8a88f2f52fac486312edc7f4cb00556953b9eecd",
]                                                                                                                      
                                                                                                                                                                                                                   
module.exports = {
	networks: {
		development: {                                                                                                                                                                                                 
			host: "127.0.0.1",     // Localhost (default: none).                                                                                                                                                          
			port: 8545,            // Standard Ethereum port (default: none)                                                                                                                                             
			network_id: "*",       // Any network (default: none)                                                                                                                                                        
			provider: new HDWalletProvider(privateKeys, "http://localhost:8545", 0, 1),                                                                                                                                      
	    },
	}
			 
};
