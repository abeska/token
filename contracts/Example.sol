//ERC20Token
pragma solidity >=0.4.22 <0.9.0; //0.5.16

contract Example{
    string public name = "MyCoin";
    string public symbol = "MYC";
    uint8 public decimal = 18;
    uint256 public supply = 1000000;
    
    event Transfer(address _receiver, address _sender, uint256 _tokens);
    event Approval(address sender, address delegate, uint256 tokens);
    
    mapping(address => uint256) public balances;
    mapping(address => mapping(address => uint256)) public allowed;
    
    constructor() public {
        balances[msg.sender] = 1000000;
    }
    
    function totalSupply() external view returns(uint256) {
        return supply;
    }
    
    function balanceOf(address tokenOwner) external view returns(uint256) {
        return balances[tokenOwner];
    }
    
    function transfer(address receiver, uint tokens) external returns(bool) {
        require(balances[msg.sender] > tokens);
        balances[msg.sender] -= tokens;
        balances[receiver] += tokens;
        emit Transfer(receiver, msg.sender, tokens);
        return true;
    }
    function approve(address delegate, uint tokens) external returns(bool) {
        allowed[msg.sender][delegate] = tokens;
        emit Approval(msg.sender, delegate, tokens);
        return true;
    }
    function allowance(address owner, address delegate) external view returns(uint256) {
        return allowed[owner][delegate];
    }
    function transferFrom(address owner, address buyer, uint256 tokens) external returns(bool) {
        require(balances[owner] >= tokens);
        require(allowed[owner][msg.sender] >= tokens);
        balances[owner] -= tokens;
        balances[buyer] += tokens;
        allowed[owner][msg.sender] -= tokens;
        emit Transfer(buyer, owner, tokens);
        return true;
    }
    
    
}
